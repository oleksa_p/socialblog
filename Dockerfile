FROM python:3
ADD . /SocialBlog/
WORKDIR /SocialBlog/
ENV FLASK_RUN_HOST=0.0.0.0
EXPOSE 5000
RUN pip install -r requirements.pip
