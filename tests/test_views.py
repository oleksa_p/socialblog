"""
Unit tests for application.
"""

from unittest import TestCase
from mock import patch, Mock
from sqlalchemy.exc import IntegrityError
from urllib.error import URLError

from app import app

from app.routes import index, post, manage, fetch_and_save, reset


class TestViews(TestCase):

    # test successful main page generation with no posts
    @patch('app.routes.models.Post')
    def test_index_empty_success(self, post_mock):
        post_mock.query.all.return_value = []
        with patch('app.routes.render_template', Mock()) as render_mock:
            index()
            render_mock.assert_called_with('index.html', title='Home', posts=[])

    # test successful main page generation with some posts
    @patch('app.routes.models.Post')
    def test_index_success(self, post_mock):
        dummy_posts = ['post1', 'post2']
        post_mock.query.all.return_value = dummy_posts
        with patch('app.routes.render_template', Mock()) as render_mock:
            index()
            render_mock.assert_called_with('index.html', title='Home', posts=dummy_posts)

    # test successful post page generation
    @patch('app.routes.models.Post')
    def test_post_success(self, post_mock):
        dummy_id = 2
        dummy_post = {'id': 2, 'body': 'post2'}
        post_mock.query.get.return_value = dummy_post
        with patch('app.routes.render_template', Mock()) as render_mock:
            post(dummy_id)
            render_mock.assert_called_with('post.html', post=dummy_post)

    # test successful manage page rendering
    def test_manage_success(self):
        with patch('app.routes.render_template', Mock()) as render_mock:
            manage()
            render_mock.assert_called_with('manage.html', title='Manage')

    # ensure successful data fetch and save
    @patch('app.routes.get_data_json', Mock(side_effect=[[], []]))
    @patch('app.routes.extract_users', Mock(return_value=[]))
    def test_fetch_and_save_success(self):
        with patch('app.routes.load_data_into_database', Mock()) as load_mock:
            with app.app_context():
                result = fetch_and_save()
                load_mock.assert_called()
        self.assertTrue('success' in str(result.data))

    # test behavior when data fetch failed
    @patch('app.routes.get_data_json', Mock(side_effect=[[], URLError]))
    @patch('app.routes.extract_users', Mock(return_value=[]))
    def test_fetch_and_save_fail_wrong_url(self):
        with patch('app.routes.load_data_into_database', Mock()) as load_mock:
            with app.app_context():
                result = fetch_and_save()
                load_mock.assert_not_called()
        self.assertTrue('failure' in str(result.data))

    # test behavior when data save failed
    @patch('app.routes.get_data_json', Mock(side_effect=[[], []]))
    @patch('app.routes.extract_users', Mock(return_value=[]))
    def test_fetch_and_save_fail_db_error(self):
        with patch('app.routes.load_data_into_database', Mock(side_effect=[[], IntegrityError])):
            with app.app_context():
                result = fetch_and_save()
        self.assertTrue('failure' in str(result.data))

    # test successfull DB clean up
    @patch('app.db.session')
    def test_reset_success(self, session_mock):
        session_mock.query().delete.side_effect = [5, 10, 15]
        with app.app_context():
            result = reset()
            session_mock.query().delete.assert_called()
        self.assertTrue('success' in str(result.data))

    # test failed DB clean up
    @patch('app.db.session')
    def test_reset_fail(self, session_mock):
        session_mock.query().delete.side_effect = [Exception, 10, Exception]
        with app.app_context():
            result = reset()
            session_mock.rollback.assert_called()
        self.assertTrue('failure' in str(result.data))
