
# README #


### Test task ###

Use Case for Python Developer from Liberty Global:

* There are users, writing posts, provided by a service:  https://jsonplaceholder.typicode.com/posts
* There are users, commenting on these posts, provided by a service: https://jsonplaceholder.typicode.com/comments
* Your code will fetch all the posts and comments, store them, and present them in a single structure.
* You are free to make your own decision and use any resource available.
* Bonus point if solution can be triggered/executed by people without Python or Linux knowledge.
* Bonus point if your solution is going to be easy to deploy and scale horizontally, in case we need to monitor many countries/API's.

### Description ###

* Provided test data analisys shows that it represents posts made by users and commented by other users.
Therefore, most convenient way to display it whould be **blog web application**.
* **Backend:** Flask framework + SQL Alchemy for DB handling. Buildin Flask web-server for demo purposes.
* **Frontend:** Very simple and minimalistic, using only basic JQuery and Bootstrap to make it look better than websites from 90s
Allows to fetch and load data into database, clear database and view data as usual blog
* **Database:** relational DB for data storage. For production environment any other than sqlite relational DB
 in separate container whould be better choice, but for test purposes sqlite is enough

### Deployment ###

##### Option 1 - local deployment ####

* clone this repository if you have git installed, or download and unpack it
* [Install pip](https://pip.pypa.io/en/stable/installing/)
* In app directory run commands in command line:
        ```
        pip install -r requirements.pip
        export FLASK_APP=app.py (for LinuxOS) or set FLASK_APP=app.py (for Windows)
        flask run
        ```
* Open the URL http://127.0.0.1:5000/ in your browser
* custom URLs for posts and comments can be specified by setting environment variables DEFAULT_POSTS_URL and DEFAULT_COMMENTS_URL

##### Option 2 - containerized deployment #####
* clone this repository if you have git installed, or download and unpack it
* install [Docker Engine](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/)
* In app directory run commands in command line:
    ```
    docker-compose up -d
    ```
* Open the URL http://127.0.0.1:5000/ in your browser
* custom URLs for posts and comments can be specified in *docker-compose.yml*

### Tests ###

App has unit tests but coverage is not 100% and it would be nice also to write functional tests.
To run tests in app directory:
    ```
    pytest tests -v
    ```
