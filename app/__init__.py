from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from app.config import Config

app = Flask(__name__)
app.config.from_object(Config)

# init and migrate database
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from app import routes, models  # pylint: disable=E402
