"""
App configuration module
"""

import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = 'incredibly secret key'

    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


# use values from env variables if provided
DEFAULT_POSTS_URL = os.environ.get('DEFAULT_POSTS_URL') or 'https://jsonplaceholder.typicode.com/posts'
DEFAULT_COMMENTS_URL = os.environ.get('DEFAULT_COMMENTS_URL') or 'https://jsonplaceholder.typicode.com/comments'
