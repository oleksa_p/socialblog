"""
Standart module includes views functions and business logic
"""

import json
from urllib.request import urlopen
from random_username.generate import generate_username
from flask import render_template, jsonify
from sqlalchemy.exc import IntegrityError
from urllib import error

from app import app, db, models
from app.config import DEFAULT_POSTS_URL, DEFAULT_COMMENTS_URL


@app.route('/')
@app.route('/index')
def index() -> str:
    posts = models.Post.query.all()
    return render_template('index.html', title='Home', posts=posts)


@app.route('/<int:post_id>')
def post(post_id: int) -> str:
    post = models.Post.query.get(post_id)
    return render_template('post.html', post=post)


@app.route('/manage')
def manage():
    return render_template('manage.html', title='Manage')


@app.route('/fetch')
def fetch_and_save() -> str:
    """
    Function view used for retrieving data from provided urls and putting it into database
    It also contains some very basic error handling, not to scare user with tracebacks
    """
    try:
        posts, comments = get_data_json(DEFAULT_POSTS_URL), get_data_json(DEFAULT_COMMENTS_URL)
    except error.URLError as url_e:
        return jsonify(
            {"status": "failure", "message": f"Failed to load data: {url_e.reason}"})
    except Exception as e:
        return jsonify(
            {"status": "failure", "message": f"Failed to load data: {e}"})

    users = extract_users(posts)

    # create a map for convenience and effective code reuse
    data_models_map = (
        (users, models.User),
        (posts, models.Post),
        (comments, models.Comment),
    )

    # iterate over map pairs and try to load each into DB
    for item in data_models_map:
        try:
            load_data_into_database(*item)
        except IntegrityError:
            return jsonify({
                "status": "failure",
                "message": f"Seems like same data is already loaded. Check Home page or delete all and try again"})
        except Exception as e:
            # import pdb; pdb.set_trace()
            return jsonify(
                {"status": "failure", "message": f"Failed to save data: {str(e)}"})

    return jsonify(
        {"status": "success", "message": f"{len(posts)} posts and {len(comments)} comments loaded to database"})


@app.route('/reset')
def reset() -> str:
    """
    Function view used for deleting data from DB
    """
    try:
        users_deleted = db.session.query(models.User).delete()
        posts_deleted = db.session.query(models.Post).delete()
        comments_deleted = db.session.query(models.Comment).delete()
        db.session.commit()

        return jsonify({
            "status": "success",
            "message": f"Deleted {users_deleted} users, {posts_deleted} posts, {comments_deleted} comments"
        })

    except Exception as e:  # this is very simplified error handling
        db.session.rollback()
        return jsonify(
            {"status": "failure", "message": f"Failed to delete data: {str(e)}"})


def get_data_json(url: str) -> list:
    """
    Simple wrapper to get json data from provided url
    Can be improved by adding extended error handling and data validation
    """
    with urlopen(url) as json_url:
        data = json.loads(json_url.read().decode())
        return data


def extract_users(posts_list: list) -> list:
    """
    Function extracts user data form provided posts list, and
    extends it with randomly generated usernames. It uses python
    comprehensions and elements of functional programming to
    demostrate candidate's awarness of it :).
    """

    # here we extract list of unique userIds from posts list
    unique_user_ids = {post['userId'] for post in posts_list}
    usernames = generate_username(len(unique_user_ids))

    # and here two lists of ids and names are transformed into list of dictiobaries
    users_list = list(
        map(
            lambda x: dict(id=x[0], username=x[1]),
            zip(unique_user_ids, usernames)))
    return users_list


def load_data_into_database(items_list: list, ModelClass: db.Model):
    """
    Simple function to load list of items into appropriate DB table
    """
    for item in items_list:
        db.session.add(ModelClass(**item))
    db.session.commit()
    return True
