"""
Standart module descrbing data models used in app
"""
from app import db


class User(db.Model):
    """
    Provided data contains only two sets, but obviously
    there is third one - users set, which can be extracted from
    first two
    """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    posts = db.relationship('Post', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140), index=True)
    body = db.Column(db.String(300))
    userId = db.Column(db.Integer, db.ForeignKey('user.id'))
    comments = db.relationship('Comment', backref='post', lazy='dynamic')

    def __repr__(self):
        return '<Post {}>'.format(self.body)

    @property
    def comments_number(self):
        return len(self.comments.all())


class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    postId = db.Column(db.Integer, db.ForeignKey('post.id'))
    name = db.Column(db.String(140), index=True)
    email = db.Column(db.String(64), index=True)
    body = db.Column(db.String(300))

    def __repr__(self):
        return '<Comment {}>'.format(self.body)

